package GUICombobox;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ComboBoxRGB extends JFrame {
	private JFrame frame;
	private JComboBox<String> box;
	private JPanel r1,b1,g1;
	private JPanel main,button,cardPanel;
	private CardLayout layoutmain;
	private String[] list = {"RED","GREEN","BLUE"};
	
	public ComboBoxRGB() {
		createframe();
	}
	
	public void createframe() {
		frame = new JFrame();
		frame.setSize(1000, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		box = new JComboBox<String>(list);
		
		main = new JPanel();
		main.setLayout(new BorderLayout());
		button = new JPanel();
		GridLayout layoutChooseCustomer = new GridLayout();
		layoutChooseCustomer.setColumns(1);
		button.setLayout(layoutChooseCustomer);
		button.add(box);
		
		main.add(button, BorderLayout.SOUTH);
		
		layoutmain = new CardLayout();
		cardPanel = new JPanel();
		cardPanel.setLayout(layoutmain);
		
		r1 = new JPanel();
		r1.setBackground(new Color(255, 0, 0));
		g1 = new JPanel();
		g1.setBackground(new Color(0, 255, 0));
		b1 = new JPanel();
		b1.setBackground(new Color(0, 0, 255));
		
		cardPanel.add(r1,"R");
		cardPanel.add(g1,"G");
		cardPanel.add(b1,"B");
		
		main.add(cardPanel,BorderLayout.CENTER);
		frame.getContentPane().add(main);
		
	}
	
	public void setListener(ActionListener addlistlistener) {
		box.addActionListener(addlistlistener);
	}
	
	public CardLayout getLayout() {
		return layoutmain;
	}
	public JPanel getCardpanel() {
		return cardPanel;
	}
	public JComboBox<String> getBox() {
		return box;
	}


}
