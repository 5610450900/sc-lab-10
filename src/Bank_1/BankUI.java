package Bank_1;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class BankUI extends JFrame {
	private JFrame frame;
	private JButton deposite,withdraw,submitdesp,submitwith,checkbalance;
	private JPanel pnDeposite,pnWithdraw,pnMain,cardPanel,pnButtonCard,pnDepositeSite,pnWithdrawSite,pnCheckBalance;
	private CardLayout card;
	private JLabel msgAct,msgDeposite,resultBalance,msgWithdraw;
	private JTextField inDeposite,inWithdraw;
	
	public BankUI() {
		createframe();
	}
	
	public void createframe() {
		//Main Frame
		frame = new JFrame();
		frame.setTitle("Bank");
		frame.setSize(600, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		pnMain = new JPanel();
		pnMain.setLayout(new BorderLayout());
		pnMain.setBorder(BorderFactory.createTitledBorder("BANK"));
		deposite = new JButton("DEPOSITE");
		withdraw = new JButton("WITHDRAW");
		checkbalance = new JButton("CHECKBALANCE");
		msgAct = new JLabel("Choose Action : ");
		pnButtonCard = new JPanel();
		GridLayout layout = new GridLayout();
		layout.setColumns(4);
		pnButtonCard.setLayout(layout);
		pnButtonCard.add(msgAct);
		pnButtonCard.add(deposite);
		pnButtonCard.add(withdraw);
		pnButtonCard.add(checkbalance);
		pnMain.add(pnButtonCard,BorderLayout.NORTH);
		pnCheckBalance = new JPanel();
		resultBalance = new JLabel();
		pnCheckBalance.add(resultBalance);
		pnMain.add(pnCheckBalance,BorderLayout.SOUTH);
		
		//Deposite
		pnDeposite = new JPanel();
		pnDeposite.setLayout(new BorderLayout());
		pnDepositeSite = new JPanel();
		msgDeposite = new JLabel("Deposite : ");
		inDeposite = new JTextField(9);
		submitdesp = new JButton("SUBMIT : DEPOSITE");
		pnDepositeSite.add(msgDeposite);
		pnDepositeSite.add(inDeposite);
		pnDepositeSite.add(submitdesp);
		pnDeposite.add(pnDepositeSite,BorderLayout.CENTER);
		
		
		//Withdraw
		pnWithdraw = new JPanel();
		pnWithdraw.setLayout(new BorderLayout());
		pnWithdrawSite = new JPanel();
		msgWithdraw = new JLabel("Withdraw : ");
		inWithdraw = new JTextField(9);
		submitwith = new JButton("SUBMIT : WITHDRAW");
		pnWithdrawSite.add(msgWithdraw);
		pnWithdrawSite.add(inWithdraw);
		pnWithdrawSite.add(submitwith);
		pnWithdraw.add(pnWithdrawSite,BorderLayout.CENTER);
		//CardPanel
		card = new CardLayout();
		cardPanel = new JPanel();
		cardPanel.setLayout(card);
		cardPanel.add(pnDeposite, "Deposite");
		cardPanel.add(pnWithdraw, "Withdraw");
		
		pnMain.add(cardPanel,BorderLayout.CENTER);
		frame.getContentPane().add(pnMain);
		
	}
	
	public CardLayout getLayout() {
		return card;
	}
	public JPanel getCardpanel() {
		return cardPanel;
	}
	
	public void setBalanceResult(String s) {
		resultBalance.setText(s);
	}
	public double getDepositeAMount() {
		return Double.parseDouble(inDeposite.getText());
	}
	public double getWithdrawAMount() {
		return Double.parseDouble(inWithdraw.getText());
	}
	public void clearTextAmount() {
		inDeposite.setText("");
		inWithdraw.setText("");
	}
	public void setlistener(ActionListener listlistener) {
		deposite.addActionListener(listlistener);
		withdraw.addActionListener(listlistener);
		submitdesp.addActionListener(listlistener);
		submitwith.addActionListener(listlistener);
		checkbalance.addActionListener(listlistener);
	}
	


}
