package GUICheckbox;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;


public class CheckboxRBG extends JFrame {
	private JFrame frame;
	private JCheckBox r,g,b;
	private JPanel r1,b1,g1,rb1,rg1,gb1,rgb1,defaultpanel;
	private JPanel main,button,cardPanel;
	private CardLayout layoutmain;
	
	public CheckboxRBG() {
		createframe();
	}
	
	public void createframe() {
		frame = new JFrame();
		frame.setSize(1000, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		r = new JCheckBox("R");
		g = new JCheckBox("G");
		b = new JCheckBox("B");
		
		main = new JPanel();
		main.setLayout(new BorderLayout());
		button = new JPanel();
		GridLayout layoutChooseCustomer = new GridLayout();
		layoutChooseCustomer.setColumns(3);
		button.setLayout(layoutChooseCustomer);
		button.add(r);
		button.add(g);
		button.add(b);
		r.setSelected(false);
		g.setSelected(false);
		b.setSelected(false);
		
		
		main.add(button, BorderLayout.SOUTH);
		
		layoutmain = new CardLayout();
		cardPanel = new JPanel();
		cardPanel.setLayout(layoutmain);
		
		defaultpanel = new JPanel();
		defaultpanel.setBackground(new Color(0, 0, 0));
		r1 = new JPanel();
		r1.setBackground(new Color(255, 0, 0));
		g1 = new JPanel();
		g1.setBackground(new Color(0, 255, 0));
		b1 = new JPanel();
		b1.setBackground(new Color(0, 0, 255));
		rg1 = new JPanel();
		rg1.setBackground(new Color(255, 255, 0));
		rb1 = new JPanel();
		rb1.setBackground(new Color(255, 0, 255));
		gb1 = new JPanel();
		gb1.setBackground(new Color(0, 255, 255));
		rgb1 = new JPanel();
		rgb1.setBackground(new Color(255, 255, 255));
		
		cardPanel.add(defaultpanel,"Default");
		cardPanel.add(r1,"R");
		cardPanel.add(g1,"G");
		cardPanel.add(b1,"B");
		cardPanel.add(rg1,"RG");
		cardPanel.add(rb1,"RB");
		cardPanel.add(gb1,"GB");
		cardPanel.add(rgb1,"RGB");
		
		
		main.add(cardPanel,BorderLayout.CENTER);
		frame.getContentPane().add(main);
		
		
	}
	
	public void setListener(ActionListener addlistlistener) {
		r.addActionListener(addlistlistener);
		g.addActionListener(addlistlistener);
		b.addActionListener(addlistlistener);
		
	}
	
	public CardLayout getLayout() {
		return layoutmain;
	}
	public JPanel getCardpanel() {
		return cardPanel;
	}
	public JCheckBox getR() {
		return r;
	}
	public JCheckBox getG() {
		return g;
	}
	public JCheckBox getB() {
		return b;
	}

}
