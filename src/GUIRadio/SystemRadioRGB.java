package GUIRadio;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class SystemRadioRGB {
	RadioRGB frame;
	public SystemRadioRGB() {
		frame = new RadioRGB();
		frame.setListener(new CheckboxActionListener());
	}
	class CheckboxActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {
			// TODO Auto-generated method stub
			switch(ae.getActionCommand()) {
			case  "R" :
				frame.getLayout().show(frame.getCardpanel(), "R");
				break;
			case "G" :
				frame.getLayout().show(frame.getCardpanel(), "G");
				break;
			case "B" :
				frame.getLayout().show(frame.getCardpanel(), "B");
				break;
			}
		}
	}
	
}
