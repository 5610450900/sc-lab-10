package GUIMenu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SystemMenuRGB {
	MenuRGB frame;
	
	public SystemMenuRGB() {
		frame = new MenuRGB();
		frame.setListener(new MenuActionListener());
	}
	class MenuActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			switch(e.getActionCommand()) {
			case  "RED" :
				frame.getLayout().show(frame.getCardpanel(), "R");
				break;
			case "GREEN" :
				frame.getLayout().show(frame.getCardpanel(), "G");
				break;
			case "BLUE" :
				frame.getLayout().show(frame.getCardpanel(), "B");
				break;
			}
			
		}
		
	}

}
